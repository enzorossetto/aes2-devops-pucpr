from flask import Flask
from flask import request

app = Flask(__name__)

@app.route("/")
def index():
    fahrenheit = request.args.get("fahrenheit", "")
    if fahrenheit:
        celsius = celsius_from(fahrenheit)
    else:
        celsius = ""

    return (
        """<h2>Converta de Fahrenheit para Celsius</h2>"""
		"""<br>"""
		"""<form action="" method="get">
                <input type="text" name="fahrenheit">
                <input type="submit" value="Converter">
            </form>"""
        + "Celsius: "
        + '<a id="celsius">' +celsius+ '</a>'

    )
 
@app.route("/<int:celsius>")
def celsius_from(fahrenheit):
    """Converte Fahrenheit para Celsius."""
    celsius = (float(fahrenheit) - 32) / 1.8
    celsius = round(celsius, 3) 
    return str(celsius)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080, debug=True)
     #app.run(host="0.0.0.0", port=8080, debug=False)
